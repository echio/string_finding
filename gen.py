# Run this file to generate the input.

import random
import string

random_string = lambda : ''.join([random.choice(string.letters) for i in xrange(100)])

with open('input.txt', 'w') as f:
    for i in xrange(100000):
        if random.randint(0, 1) == 0:
            f.write('abc' + random_string()[:97])
        else:
            f.write(random_string())
        f.write('\n')
